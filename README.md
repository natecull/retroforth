# retroforth

Charles Childers' RETRO Forth 12 ( http://retroforth.org/ ) is an exciting little Forth which for some reason hits both my nostalgia and elegance buttons hard. Running on a 32-bit virtual machine called Nga with only 30-ish instructions, and with almost everything about it user-facing and hackable, Retro is just begging to be tweaked into the development environment of your dreams.

Being a Forth (but not an ANSI Forth, it's borrowed several constructs from ColorForth, like prefixes, and Joy/Factor, like quotations and combinators) and with a hookable 'interpret' function, it stands a pretty good chance of being able to be turned into a universal language.

This repository contains some projects I've been doing with Retro.

* `modal` is a user interface toolkit based around hacking the Retro parser itself to be a modal visual interface, even for extremely restricted virtual machines with just basic text input/output

* `random-parker-miller-carta` is an implementation of the David Carter / Robin Whittle optimisation of the Parker-Miller Minimal Standard random number generator (multiply by 16807 and mod by 2147483647; that part's simple, Carta's deeply weird 1990 mathematical trick, unearthed and documented by Whittle in the late 2000s, is how to perform that multiplication on a platform that doesn't have 64 bit numbers available). It uses one word of state and fits nicely in 31 bit signed integers and generates a full cycle of 2 billion-ish numbers. 






